/*
 * SPDX-FileCopyrightText: 2023 Lexxy Fox <PGP 7E6DDC75EC1AA80F>
 * SPDX-FileCopyrightText: 1999-2011 Brian Raiter <breadbox@muppetlabs.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <elf.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "bswap.h"
#include "elfrw.h"

bool elfrw_read_ehdr(FILE *restrict fp, Elf64_Ehdr *in) {
	if (fread(in->e_ident, EI_NIDENT, 1, fp) != 1) return false;
	if (elfrw_initialize_ident(in->e_ident) != 0) return false;
	if (is64bit_form()) {
		if (!fread((char *)in + EI_NIDENT, sizeof *in - EI_NIDENT, 1, fp))
			return false;
		if (native_form()) return true;
		bswap16(&in->e_type);
		bswap16(&in->e_machine);
		bswap32(&in->e_version);
		bswap64(&in->e_entry);
		bswap64(&in->e_phoff);
		bswap64(&in->e_shoff);
		bswap32(&in->e_flags);
		bswap16(&in->e_ehsize);
		bswap16(&in->e_phentsize);
		bswap16(&in->e_phnum);
		bswap16(&in->e_shentsize);
		bswap16(&in->e_shnum);
		bswap16(&in->e_shstrndx);
		return true;
	}
	Elf32_Ehdr in32;
	if (fread((char *)&in32 + EI_NIDENT, sizeof in32 - EI_NIDENT, 1, fp) != 1)
		return false;
	if (native_form()) {
		in->e_type = in32.e_type;
		in->e_machine = in32.e_machine;
		in->e_version = in32.e_version;
		in->e_entry = in32.e_entry;
		in->e_phoff = in32.e_phoff;
		in->e_shoff = in32.e_shoff;
		in->e_flags = in32.e_flags;
		in->e_ehsize = in32.e_ehsize;
		in->e_phentsize = in32.e_phentsize;
		in->e_phnum = in32.e_phnum;
		in->e_shentsize = in32.e_shentsize;
		in->e_shnum = in32.e_shnum;
		in->e_shstrndx = in32.e_shstrndx;
	} else {
		in->e_type = bswapped16(in32.e_type);
		in->e_machine = bswapped16(in32.e_machine);
		in->e_version = bswapped32(in32.e_version);
		in->e_entry = bswapped32(in32.e_entry);
		in->e_phoff = bswapped32(in32.e_phoff);
		in->e_shoff = bswapped32(in32.e_shoff);
		in->e_flags = bswapped32(in32.e_flags);
		in->e_ehsize = bswapped16(in32.e_ehsize);
		in->e_phentsize = bswapped16(in32.e_phentsize);
		in->e_phnum = bswapped16(in32.e_phnum);
		in->e_shentsize = bswapped16(in32.e_shentsize);
		in->e_shnum = bswapped16(in32.e_shnum);
		in->e_shstrndx = bswapped16(in32.e_shstrndx);
	}
	return true;
}

bool elfrw_write_ehdr(FILE *restrict fp, Elf64_Ehdr const *out) {
	if (elfrw_initialize_ident(out->e_ident)) return false;
	if (is64bit_form()) {
		if (native_form()) return fwrite(out, sizeof *out, 1, fp);
		Elf64_Ehdr outrev;
		memcpy(outrev.e_ident, out->e_ident, EI_NIDENT);
		outrev.e_type = bswapped16(out->e_type);
		outrev.e_machine = bswapped16(out->e_machine);
		outrev.e_version = bswapped32(out->e_version);
		outrev.e_entry = bswapped64(out->e_entry);
		outrev.e_phoff = bswapped64(out->e_phoff);
		outrev.e_shoff = bswapped64(out->e_shoff);
		outrev.e_flags = bswapped32(out->e_flags);
		outrev.e_ehsize = bswapped16(out->e_ehsize);
		outrev.e_phentsize = bswapped16(out->e_phentsize);
		outrev.e_phnum = bswapped16(out->e_phnum);
		outrev.e_shentsize = bswapped16(out->e_shentsize);
		outrev.e_shnum = bswapped16(out->e_shnum);
		outrev.e_shstrndx = bswapped16(out->e_shstrndx);
		return fwrite(&outrev, sizeof outrev, 1, fp);
	}
	Elf32_Ehdr out32;
	if (native_form()) {
		memcpy(out32.e_ident, out->e_ident, EI_NIDENT);
		out32.e_type = out->e_type;
		out32.e_machine = out->e_machine;
		out32.e_version = out->e_version;
		out32.e_entry = out->e_entry;
		out32.e_phoff = out->e_phoff;
		out32.e_shoff = out->e_shoff;
		out32.e_flags = out->e_flags;
		out32.e_ehsize = out->e_ehsize;
		out32.e_phentsize = out->e_phentsize;
		out32.e_phnum = out->e_phnum;
		out32.e_shentsize = out->e_shentsize;
		out32.e_shnum = out->e_shnum;
		out32.e_shstrndx = out->e_shstrndx;
	} else {
		memcpy(out32.e_ident, out->e_ident, EI_NIDENT);
		out32.e_type = bswapped16(out->e_type);
		out32.e_machine = bswapped16(out->e_machine);
		out32.e_version = bswapped32(out->e_version);
		out32.e_entry = bswapped32(out->e_entry);
		out32.e_phoff = bswapped32(out->e_phoff);
		out32.e_shoff = bswapped32(out->e_shoff);
		out32.e_flags = bswapped32(out->e_flags);
		out32.e_ehsize = bswapped16(out->e_ehsize);
		out32.e_phentsize = bswapped16(out->e_phentsize);
		out32.e_phnum = bswapped16(out->e_phnum);
		out32.e_shentsize = bswapped16(out->e_shentsize);
		out32.e_shnum = bswapped16(out->e_shnum);
		out32.e_shstrndx = bswapped16(out->e_shstrndx);
	}
	return fwrite(&out32, sizeof out32, 1, fp);
}
