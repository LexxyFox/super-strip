#pragma once

#include <elf.h>
#include <stdbool.h>
#include <stdio.h>

/**
 * Reads an ELF header from `fp` and initializes the ELFRW global state.
 * Returns `true` on success, `false` on failure.
 */
bool elfrw_read_ehdr(FILE *fp, Elf64_Ehdr *in);

/**
 * Loads an ELF header into the ELFRW global state and writes it to `fp`.
 * Returns `true` on success, `false` on failure.
 */
bool elfrw_write_ehdr(FILE *fp, Elf64_Ehdr const *out);