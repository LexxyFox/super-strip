#pragma once

#include <stdint.h>

/* The library's current settings. */
extern uint8_t elfrw_native_data;
extern uint8_t elfrw_current_class;
extern uint8_t elfrw_current_data;
extern uint8_t elfrw_current_version;

/* Macros that encapsulate the commonly needed tests. */
#define native_form()  (elfrw_native_data == elfrw_current_data)
#define is64bit_form() (elfrw_current_class == ELFCLASS64)

/*
 * Sets the ELFRW global state using the provided values.
 * Returns `0` on success, or a negative value if the values are invalid.
 */
int elfrw_initialize_direct(
		uint8_t const clas, uint8_t const data, uint8_t const version
);

/*
 * Sets the ELFRW global state using the provided ELF identifier value.
 * Returns `0` on success, or a negative value if the values are invalid.
 */
int elfrw_initialize_ident(uint8_t const *ident);
