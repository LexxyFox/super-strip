/*
 * SPDX-FileCopyrightText: 2023 Lexxy Fox <PGP 7E6DDC75EC1AA80F>
 * SPDX-FileCopyrightText: 1999-2011 Brian Raiter <breadbox@muppetlabs.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <stdint.h>

/* Endianness-swapping functions. */

/** Swaps the byte endianness of a two byte value. */
static inline uint16_t bswapped16(const uint16_t in) {
	return (uint16_t)((in & 0x00FF) << 8) | ((in & 0xFF00) >> 8);
}

/** Swaps the byte endianness of a four byte value. */
static inline uint32_t bswapped32(uint32_t in) {
#if defined __i386__ || defined __x86_64__
	__asm__("bswap %0" : "=r"(in) : "0"(in));
	return in;
#else
	return ((in & 0x000000FFU) << 24) | ((in & 0x0000FF00U) << 8) |
	       ((in & 0x00FF0000U) >> 8) | ((in & 0xFF000000U) >> 24);
#endif
}

/** Swaps the byte endianness of an eight byte value. */
static inline uint64_t bswapped64(uint64_t in) {
#if defined __x86_64__
	__asm__("bswap %0" : "=r"(in) : "0"(in));
	return in;
#else
	return ((in & 0x00000000000000FFULL) << 56) |
	       ((in & 0x000000000000FF00ULL) << 40) |
	       ((in & 0x0000000000FF0000ULL) << 24) |
	       ((in & 0x00000000FF000000ULL) << 8) |
	       ((in & 0x000000FF00000000ULL) >> 8) |
	       ((in & 0x0000FF0000000000ULL) >> 24) |
	       ((in & 0x00FF000000000000ULL) >> 40) |
	       ((in & 0xFF00000000000000ULL) >> 56);
#endif
}

/** Swaps the byte endianness of a two byte value in place. */
static inline void bswap16(void *in) {
	uint8_t tmp = ((uint8_t *)in)[0];
	((uint8_t *)in)[0] = ((uint8_t *)in)[1];
	((uint8_t *)in)[1] = tmp;
}

/** Swaps the byte endianness of a two byte value in place. */
static inline void bswap32(void *in) {
#if defined __i386__ || defined __x86_64__
	__asm__("bswap %0" : "=r"(*(uint32_t *)in) : "0"(*(uint32_t *)in));
#else
	uint8_t tmp = ((uint8_t *)in)[0];
	((uint8_t *)in)[0] = ((uint8_t *)in)[3];
	((uint8_t *)in)[3] = tmp;
	tmp = ((uint8_t *)in)[1];
	((uint8_t *)in)[1] = ((uint8_t *)in)[2];
	((uint8_t *)in)[2] = tmp;
#endif
}

static inline void bswap64(void *in) {
#if defined __x86_64__
	__asm__("bswap %0" : "=r"(*(uint64_t *)in) : "0"(*(uint64_t *)in));
#else
	uint8_t tmp = ((uint8_t *)in)[0];
	((uint8_t *)in)[0] = ((uint8_t *)in)[7];
	((uint8_t *)in)[7] = tmp;
	tmp = ((uint8_t *)in)[1];
	((uint8_t *)in)[1] = ((uint8_t *)in)[6];
	((uint8_t *)in)[6] = tmp;
	tmp = ((uint8_t *)in)[2];
	((uint8_t *)in)[2] = ((uint8_t *)in)[5];
	((uint8_t *)in)[5] = tmp;
	tmp = ((uint8_t *)in)[3];
	((uint8_t *)in)[3] = ((uint8_t *)in)[4];
	((uint8_t *)in)[4] = tmp;
#endif
}
