#pragma once

#include <elf.h>
#include <stdbool.h>
#include <stdio.h>

/**
 * Reads an ELF program header from FILE stream `fp`.
 * Returns `true` on success, `false` on failure.
 */
bool elfrw_read_phdr(FILE *fp, Elf64_Phdr *in);
int elfrw_read_phdrs(FILE *fp, Elf64_Phdr *in, const int count);

/**
 * Writes an ELF program header to FILE stream `fp`.
 * Returns `true` on success, `false` on failure.
 */
bool elfrw_write_phdr(FILE *fp, Elf64_Phdr const *out);
int elfrw_write_phdrs(FILE *fp, Elf64_Phdr const *out, const int count);