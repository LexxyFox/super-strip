/*
 * SPDX-FileCopyrightText: 2023 Lexxy Fox <PGP 7E6DDC75EC1AA80F>
 * SPDX-FileCopyrightText: 1999-2011 Brian Raiter <breadbox@muppetlabs.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <errno.h>
#include <getopt.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "elfrw.c"
#include "elfrw_ehdr.c"
#include "elfrw_phdr.c"

#define xstr(s) str(s)
#define str(s) #s

/* The name of the program. */
#ifndef __USE_GNU
const char *program_invocation_name;
#endif /* __USE_GNU */

/* true if we should attempt to truncate zero bytes from the end of the file. */
static bool dozerotrunc = false;

/* Information for each executable operated upon. */
static const char *thefilename; /* the name of the current file */
static FILE *thefile;           /* the currently open file handle */
static Elf64_Ehdr ehdr;         /* the current file's ELF header */
static Elf64_Phdr *phdrs;       /* the program segment header table */
static uint64_t newsize;        /* the proposed new file size */

/* A simple error-handling function. false is always returned for the
 * convenience of the caller. */
static bool err(char const *errmsg) {
	fprintf(stderr, "%s: %s: %s\n", program_invocation_name, thefilename, errmsg);
	return false;
}

/* A macro for I/O errors: The given error message is used only when errno is
 * not set. */
#define ferr(msg) (err(ferror(thefile) ? strerror(errno) : (msg)))

/* readelfheader() reads the ELF header into our global variable, and checks to
 * make sure that this is in fact a file that we should be munging. */
static bool readelfheader(void) {
	if (!elfrw_read_ehdr(thefile, &ehdr)) return ferr("not a valid ELF file");

	if (ehdr.e_type != ET_EXEC && ehdr.e_type != ET_DYN)
		return err("not an executable or shared-object library.");

	return true;
}

/* readphdrtable() loads the program segment header table into memory. */
static bool readphdrtable(void) {
	if (!ehdr.e_phoff || !ehdr.e_phnum)
		return err("ELF file has no program header table.");

	if (!(phdrs = realloc(phdrs, ehdr.e_phnum * sizeof *phdrs)))
		return err("Out of memory!");
	if (elfrw_read_phdrs(thefile, phdrs, ehdr.e_phnum) != ehdr.e_phnum)
		return ferr("missing or incomplete program segment header table.");

	return true;
}

/* getmemorysize() determines the offset of the last byte of the file that is
 * referenced by an entry in the program segment header table. Anything in the
 * file after that point is not used when the program is executing, and thus can
 * be safely discarded. */
static bool getmemorysize(void) {
	uint64_t size, n;

	/* Start by setting the size to include the ELF header and the complete
	 * program segment header table. */
	size = ehdr.e_phoff + ehdr.e_phnum * sizeof *phdrs;
	if (size < ehdr.e_ehsize) size = ehdr.e_ehsize;

	/* Then keep extending the size to include whatever data the program segment
	 * header table references. */
	for (Elf64_Half i = 0; i < ehdr.e_phnum; ++i) {
		if (phdrs[i].p_type != PT_NULL) {
			n = phdrs[i].p_offset + phdrs[i].p_filesz;
			if (n > size) size = n;
		}
	}

	newsize = size;
	return true;
}

/* truncatezeros() examines the bytes at the end of the file's size-to-be, and
 * reduces the size to exclude any trailing zero bytes. */
static bool truncatezeros(void) {
	if (!dozerotrunc) return true;

	uint8_t contents[1024];
	uint64_t size, n;

	size = newsize;
	do {
		n = sizeof contents;
		if (n > size) n = size;
		if (fseek(thefile, size - n, SEEK_SET)) return ferr("cannot seek in file.");
		if (fread(contents, n, 1, thefile) != 1)
			return ferr("cannot read file contents");
		while (n && !contents[--n]) --size;
	} while (size && !n);

	/* Sanity check. */
	if (!size) return err("ELF file is completely blank!");

	newsize = size;
	return true;
}

/* modifyheaders() removes references to the section header table if it was
 * stripped, and reduces program header table entries that included truncated
 * bytes at the end of the file. */
static bool modifyheaders(void) {
	/* If the section header table is gone, then remove all references to it in
	 * the ELF header. */
	if (ehdr.e_shoff >= newsize) {
		ehdr.e_shoff = 0;
		ehdr.e_shnum = 0;
		ehdr.e_shstrndx = 0;
	}

	/* The program adjusts the file size of any segment that was truncated. The
	 * case of a segment being completely stripped out is handled separately. */
	for (Elf64_Half i = 0; i < ehdr.e_phnum; ++i) {
		if (phdrs[i].p_offset >= newsize) {
			phdrs[i].p_offset = newsize;
			phdrs[i].p_filesz = 0;
		} else if (phdrs[i].p_offset + phdrs[i].p_filesz > newsize) {
			phdrs[i].p_filesz = newsize - phdrs[i].p_offset;
		}
	}

	return true;
}

/* commitchanges() writes the new headers back to the original file and sets the
 * file to its new size. */
static bool commitchanges(void) {
	/* Save the changes to the ELF header, if any. */
	if (fseek(thefile, 0, SEEK_SET)) return ferr("could not rewind file");
	if (!elfrw_write_ehdr(thefile, &ehdr)) return ferr("could not modify file");

	/* Save the changes to the program segment header table, if any. */
	if (fseek(thefile, ehdr.e_phoff, SEEK_SET)) {
		ferr("could not seek in file");
		goto warning;
	}
	if (elfrw_write_phdrs(thefile, phdrs, ehdr.e_phnum) != ehdr.e_phnum) {
		ferr("could not write to file");
		goto warning;
	}

	/* Eleventh-hour sanity check: don't truncate before the end of the program
	 * segment header table. */
	uint32_t n = ehdr.e_phnum * ehdr.e_phentsize;
	if (newsize < ehdr.e_phoff + n) newsize = ehdr.e_phoff + n;

	/* Chop off the end of the file. */
	if (ftruncate(fileno(thefile), newsize)) {
		err(errno ? strerror(errno) : "could not resize file");
		goto warning;
	}

	return true;

warning:
	return err("ELF file may have been corrupted!");
}

/* main() loops over the cmdline arguments, leaving all the real work to the
 * other functions. */
int main(int argc, char *argv[]) {
#ifndef __USE_GNU
	program_invocation_name = argv[0];
#endif /* __USE_GNU */

	int opt = EOF;
	if (argc <= 1) goto print_usage;

	while ((opt = getopt_long(
							argc, argv, "hVz",
							(struct option[]
	            ){{"zeros", no_argument, NULL, 'z'},
	              {"zeroes", no_argument, NULL, 'z'},
	              {"help", no_argument, NULL, 'h'},
	              {"version", no_argument, NULL, 'V'},
	              {}},
							NULL
					)) != EOF) {
		switch (opt) {
			case 'h':
			print_usage:
				fprintf(
						stdout,
						"Usage: %s [OPTIONS] FILE...\n"
						"Remove all nonessential bytes from executable ELF files.\n\n"
						"  -z, --zeroes        Also discard trailing zero bytes.\n"
						"  -h, --help          Display this help and exit.\n"
						"  -V, --version       Print program version.\n",
						program_invocation_name
				);
				break;
			case 'V':
				fputs("sstrip, version " xstr(SSTRIP_VERSION) "\n", stdout);
				break;
			case 'z':
				dozerotrunc = true;
				break;
			default:
				fprintf(
						stderr, "Try '%s --help' for more information.\n",
						program_invocation_name
				);
				exit(EXIT_FAILURE);
		}
	}

	int failures = 0;

	while (optind < argc) {
		thefilename = argv[optind++];
		thefile = fopen(thefilename, "rb+");
		if (!thefile) {
			err(strerror(errno));
			++failures;
			continue;
		}

		if (!(readelfheader() && readphdrtable() && getmemorysize() &&
		      truncatezeros() && modifyheaders() && commitchanges()))
			++failures;

		fclose(thefile);
	}

	return failures;
}
