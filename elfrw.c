/*
 * SPDX-FileCopyrightText: 2023 Lexxy Fox <PGP 7E6DDC75EC1AA80F>
 * SPDX-FileCopyrightText: 1999-2011 Brian Raiter <breadbox@muppetlabs.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <elf.h>

#include "elfrw.h"

/* The library's current settings. */
uint8_t elfrw_native_data;
uint8_t elfrw_current_class;
uint8_t elfrw_current_data;
uint8_t elfrw_current_version;

/*
 * Sets the ELFRW global state using the provided values.
 * Returns `0` on success, or a negative value if the values are invalid.
 */
int elfrw_initialize_direct(
		uint8_t const class, uint8_t const data, uint8_t const version
) {
	if (!elfrw_native_data) {
		int msb = 1;
		*(char *)&msb = 0;
		elfrw_native_data = msb ? ELFDATA2MSB : ELFDATA2LSB;
	}

	switch (class) {
		case ELFCLASS32:
		case ELFCLASS64:
			elfrw_current_class = class;
			break;
		default:
			return -EI_CLASS;
	}

	switch (data) {
		case ELFDATA2LSB:
		case ELFDATA2MSB:
			elfrw_current_data = data;
			break;
		default:
			return -EI_DATA;
	}

	elfrw_current_version = version;
	if (elfrw_current_version != EV_CURRENT) return -EI_VERSION;

	return 0;
}

/*
 * Sets the ELFRW global state using the provided ELF identifier value.
 * Returns `0` on success, or a negative value if the values are invalid.
 */
int elfrw_initialize_ident(uint8_t const *ident) {
	if (ident[EI_MAG0] != ELFMAG0 || ident[EI_MAG1] != ELFMAG1 ||
	    ident[EI_MAG2] != ELFMAG2 || ident[EI_MAG3] != ELFMAG3)
		return -1;
	return elfrw_initialize_direct(
			ident[EI_CLASS], ident[EI_DATA], ident[EI_VERSION]
	);
}
