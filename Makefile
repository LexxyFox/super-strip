# Options: (ex: `CFLAGS=-D_GNU_SOURCE make`)
#   _GNU_SOURCE: enable use of GNU extensions

SSTRIP_VERSION ?= 3.1.4

prefix ?= /usr/local
exec_prefix ?= $(prefix)
bindir ?= $(exec_prefix)/bin
datarootdir ?= $(prefix)/share
datadir ?= $(datarootdir)
mandir ?= $(datarootdir)/man
man1dir ?= $(mandir)/man1

GZIP_PROG ?= gzip -cn9
INSTALL ?= install -D
INSTALL_PROGRAM ?= $(INSTALL)
INSTALL_DATA ?= $(INSTALL) -m 644
M4 ?= m4
RM ?= rm -f

CPPFLAGS := \
	-D_FORTIFY_SOURCE=2 \
	-DDATE=$(shell date '+%Y-%m-%d') \
	-DSSTRIP_VERSION=$(SSTRIP_VERSION) \
	$(CPPFLAGS)
OFLAGS := -O3 \
	-falign-functions=1 \
	-falign-jumps=1 \
	-falign-loops=1 \
	-fmerge-all-constants \
	-fno-asynchronous-unwind-tables \
	-fno-ident \
	-fno-plt \
	-fno-stack-protector \
	-fno-unwind-tables \
	-fomit-frame-pointer \
	-fwhole-program \
	$(OFLAGS)
WFLAGS := \
	-Wall \
	-Werror \
	-Wextra \
	-Wformat-security \
	-Wno-ignored-optimization-argument \
	-Wno-unknown-warning-option \
	-Wsuggest-attribute=const \
	-Wsuggest-attribute=format \
	-Wsuggest-attribute=noreturn \
	-Wsuggest-attribute=pure \
	$(WFLAGS)
CFLAGS := \
	-g0 \
	-MD \
	-MP \
	-pipe \
	$(OFLAGS) \
	$(WFLAGS) \
	$(CFLAGS)
LDFLAGS := \
	-Wl,--build-id=none \
	-Wl,--gc-sections \
	-Wl,-znoexecstack \
	-Wl,-znorelro \
	-Wl,-znoseparate-code \
	$(LDFLAGS)
MAKEFLAGS := \
	--jobs=$(shell nproc) \
	$(MAKEFLAGS)

.PHONY: all clean install install-man install-strip uninstall

all: sstrip

-include sstrip.d

sstrip.1.gz: sstrip.1.in
	$(M4) $(CPPFLAGS) $< | $(GZIP_PROG) > $@

clean:
	$(RM) sstrip sstrip.1 sstrip.1.gz sstrip.i sstrip.s sstrip.o

$(DESTDIR)$(bindir)/sstrip: sstrip
	$(INSTALL_PROGRAM) $< $@

$(DESTDIR)$(man1dir)/sstrip.1.gz: sstrip.1.gz
	$(INSTALL_DATA) $< $@

install-man: $(DESTDIR)$(man1dir)/sstrip.1.gz

install: $(DESTDIR)$(bindir)/sstrip install-man

install-strip: install
	./sstrip -z $(DESTDIR)$(bindir)/sstrip

uninstall:
	$(RM) $(DESTDIR)$(bindir)/sstrip $(DESTDIR)$(man1dir)/sstrip.1.gz

# References:
# https://www.gnu.org/prep/standards/html_node/Command-Variables.html
# https://www.gnu.org/prep/standards/html_node/Directory-Variables.html
# https://www.gnu.org/prep/standards/html_node/Standard-Targets.html
# https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html
# https://www.gnu.org/software/make/manual/html_node/Implicit-Variables.html