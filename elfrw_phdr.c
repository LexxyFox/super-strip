/*
 * SPDX-FileCopyrightText: 2023 Lexxy Fox <PGP 7E6DDC75EC1AA80F>
 * SPDX-FileCopyrightText: 1999-2011 Brian Raiter <breadbox@muppetlabs.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <elf.h>
#include <stdbool.h>
#include <stdio.h>

#include "bswap.h"
#include "elfrw.h"

bool elfrw_read_phdr(FILE *restrict fp, Elf64_Phdr *in) {
	if (is64bit_form()) {
		if (fread(in, sizeof *in, 1, fp) != 1) return false;
		if (native_form()) return true;
		bswap32(&in->p_type);
		bswap32(&in->p_flags);
		bswap64(&in->p_offset);
		bswap64(&in->p_vaddr);
		bswap64(&in->p_paddr);
		bswap64(&in->p_filesz);
		bswap64(&in->p_memsz);
		bswap64(&in->p_align);
		return true;
	}
	Elf32_Phdr in32;
	if (fread(&in32, sizeof in32, 1, fp) != 1) return false;
	if (native_form()) {
		in->p_type = in32.p_type;
		in->p_flags = in32.p_flags;
		in->p_offset = in32.p_offset;
		in->p_vaddr = in32.p_vaddr;
		in->p_paddr = in32.p_paddr;
		in->p_filesz = in32.p_filesz;
		in->p_memsz = in32.p_memsz;
		in->p_align = in32.p_align;
	} else {
		in->p_type = bswapped32(in32.p_type);
		in->p_offset = bswapped32(in32.p_offset);
		in->p_vaddr = bswapped32(in32.p_vaddr);
		in->p_paddr = bswapped32(in32.p_paddr);
		in->p_filesz = bswapped32(in32.p_filesz);
		in->p_memsz = bswapped32(in32.p_memsz);
		in->p_flags = bswapped32(in32.p_flags);
		in->p_align = bswapped32(in32.p_align);
	}
	return true;
}

int elfrw_read_phdrs(FILE *restrict fp, Elf64_Phdr *in, const int count) {
	int i;

	for (i = 0; i < count; ++i)
		if (!elfrw_read_phdr(fp, &in[i])) break;
	return i;
}

bool elfrw_write_phdr(FILE *restrict fp, Elf64_Phdr const *out) {
	if (is64bit_form()) {
		if (native_form()) return fwrite(out, sizeof *out, 1, fp) == 1;
		Elf64_Phdr outrev;
		outrev.p_type = bswapped32(out->p_type);
		outrev.p_flags = bswapped32(out->p_flags);
		outrev.p_offset = bswapped64(out->p_offset);
		outrev.p_vaddr = bswapped64(out->p_vaddr);
		outrev.p_paddr = bswapped64(out->p_paddr);
		outrev.p_filesz = bswapped64(out->p_filesz);
		outrev.p_memsz = bswapped64(out->p_memsz);
		outrev.p_align = bswapped64(out->p_align);
		return fwrite(&outrev, sizeof outrev, 1, fp) == 1;
	}
	Elf32_Phdr out32;
	if (native_form()) {
		out32.p_type = out->p_type;
		out32.p_offset = out->p_offset;
		out32.p_vaddr = out->p_vaddr;
		out32.p_paddr = out->p_paddr;
		out32.p_filesz = out->p_filesz;
		out32.p_memsz = out->p_memsz;
		out32.p_flags = out->p_flags;
		out32.p_align = out->p_align;
	} else {
		out32.p_type = bswapped32(out->p_type);
		out32.p_offset = bswapped32(out->p_offset);
		out32.p_vaddr = bswapped32(out->p_vaddr);
		out32.p_paddr = bswapped32(out->p_paddr);
		out32.p_filesz = bswapped32(out->p_filesz);
		out32.p_memsz = bswapped32(out->p_memsz);
		out32.p_flags = bswapped32(out->p_flags);
		out32.p_align = bswapped32(out->p_align);
	}
	return fwrite(&out32, sizeof out32, 1, fp) == 1;
}

int elfrw_write_phdrs(FILE *restrict fp, Elf64_Phdr const *out, const int count) {
	int i;

	for (i = 0; i < count; ++i)
		if (!elfrw_write_phdr(fp, &out[i])) break;
	return i;
}
